#include <chrono> // Date and time
#include <functional> // Arithmetic, comparisons, and logical operations
#include <memory> // Dynamic memory management
#include <string> // String functions
#include "rclcpp/rclcpp.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

// Trajectory generator node 
class TrajGen : public rclcpp::Node
{
  public:
    TrajGen():
      Node("traj_gen")
      {      
      }
 
  private:
};
 
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<TrajGen>());  
  rclcpp::shutdown();
  return 0;
}