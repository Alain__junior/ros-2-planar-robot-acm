# ros2_planar_robot

## November $14^{th}$ 2023

### Develop a `trajectory_generator` node complying with the following characteristics:
- Subscribed to a topic `/nxt_pose`, receiving messages with type `RefPose`. This type should have the fields:
    - `x`;
    - `y`;
    - `deltat`;

- Publisher in a topic named `/desired_state`, in which all the data from the `trajectory_generator` necessary to the controller is streamed. The streamed messages should have type `CartesianState`;

- When created, the node should not stream any data, since it does not has access to the initial pose;

- When a first `RefPose` is received, the note starts to stream;

- When remaining `RefPoses` are received, a trajectory should be streamed taking the previous on as initial pose.

### Intermediary goals:

- Edit `CMakeLists.txt` and create `.msg` files in folder `msg` such that:

    - The executable `trajectory_generator` has access to the `trajectory_generation_lib` library (in computation and runtime);

    - The node publishes and is subscribed as indicated in the description above.

------------------------------------------------------------------------------------

### Answers

#### Write here the instructions to demonstrate the functionality of the developed solutions:

1st terminal:
```
cd ros_workspace
source /opt/ros/...
colcon build
ros2 run ros2_planar_robot
```

2nd terminal:
```
ros2 topic echo etc...
```

3rd terminal:
```
ros2 topic pub etc...
```
